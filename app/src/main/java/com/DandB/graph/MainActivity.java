package com.DandB.graph;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import net.objecthunter.exp4j.ValidationResult;
import net.objecthunter.exp4j.tokenizer.UnknownFunctionOrVariableException;

import java.util.List;

public class MainActivity extends AppCompatActivity {


    AlertDialog.Builder ad;
    Context context;
    EditText formula1;
    String Prog, Prog2d;
    boolean normal = true;
    boolean polar = false;
    boolean normal2d = true;
    boolean polar2d = false;
    EditText min, min2d;
    EditText max, max2d;
    EditText formul1, formul2, formul3;
    int tab1;
    int[] idedit;
    int num = 0;
    int tnum;
    SeekBar progres;
    LinearLayout d2lay;
    ImageButton add;
    ImageButton del;
    EditText formula2;
    SeekBar progres2;
    static final private int CHOOSE_THIEF = 0;
    TabHost tabHost;
    boolean contin;
    int qrnum=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);

        context = MainActivity.this;
        formula1 = (EditText) findViewById(R.id.editText);
        min = (EditText) findViewById(R.id.editText2);
        max = (EditText) findViewById(R.id.editText3);
        min.setText("0");
        max.setText("0");
        min2d = (EditText) findViewById(R.id.editText22d);
        max2d = (EditText) findViewById(R.id.editText32d);
        min2d.setText("0");
        max2d.setText("0");
        Prog = "0";
        Prog2d = "0";
        progres = (SeekBar) findViewById(R.id.seekBar);
        d2lay = (LinearLayout) findViewById(R.id.linearLayout2d);
        add = (ImageButton) findViewById(R.id.imageButton3);
        del = (ImageButton) findViewById(R.id.imageButton4);
        formula2 = (EditText) findViewById(R.id.editText2d);
        progres2 = (SeekBar) findViewById(R.id.seekBar2d);
        del.setVisibility(View.INVISIBLE);
        final ImageButton addbutton = (ImageButton) findViewById(R.id.imageButton3);

        tabHost = (TabHost) findViewById(R.id.tabHost);

        tabHost.setup();

        TabHost.TabSpec tabSpec = tabHost.newTabSpec("tag1");

        tabSpec.setContent(R.id.tab1);
        tabSpec.setIndicator("2D");
        tabHost.addTab(tabSpec);

        tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab()).setBackgroundColor(Color.parseColor("#2ec96f"));
        TextView tv = (TextView) tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab()).findViewById(android.R.id.title);
        tv.setTextColor(Color.WHITE);
        tv.setTextSize(20);

        tabSpec = tabHost.newTabSpec("tag2");
        tabSpec.setContent(R.id.tab2);
        tabSpec.setIndicator("3D");
        tabHost.addTab(tabSpec);

        TextView tv1 = (TextView) tabHost.getTabWidget().getChildAt(1).findViewById(android.R.id.title);
        tv1.setTextColor(Color.WHITE);
        tv1.setTextSize(20);


        /*tabSpec = tabHost.newTabSpec("tag3");
        tabSpec.setContent(R.id.tab3);
        tabSpec.setIndicator("Котёнок");
        tabHost.addTab(tabSpec);*/
        tabHost.setCurrentTab(0);
        tab1 = tabHost.getCurrentTab();


        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                int tab = tabHost.getCurrentTab();
                for (int i = 0; i < tabHost.getTabWidget().getChildCount(); i++) {
                    // When tab is not selected
                    tabHost.getTabWidget().getChildAt(i).setBackgroundColor(Color.parseColor("#248e4f"));
                    TextView tv = (TextView) tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
                    tv.setTextColor(Color.WHITE);
                }
                // When tab is selected
                tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab()).setBackgroundColor(Color.parseColor("#2ec96f"));
                TextView tv = (TextView) tabHost.getTabWidget().getChildAt(tab).findViewById(android.R.id.title);
                tv.setTextColor(Color.WHITE);
                tab1 = tabHost.getCurrentTab();
            }
        });


        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroup);

        RadioGroup radioGroup2d = (RadioGroup) findViewById(R.id.radioGroup2d);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case -1:
                        break;
                    case R.id.radioButton:
                        normal = true;
                        polar = false;

                        break;
                    case R.id.radioButton2:
                        normal = false;
                        polar = true;

                        break;

                    default:
                        break;
                }
            }
        });

        radioGroup2d.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case -1:
                        break;
                    case R.id.radioButton2d:
                        normal2d = true;
                        polar2d = false;
                        addbutton.setVisibility(View.VISIBLE);
                        break;
                    case R.id.radioButton22d:
                        normal2d = false;
                        polar2d = true;
                        addbutton.setVisibility(View.INVISIBLE);
                        break;

                    default:
                        break;
                }
            }
        });

        progres.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {


                min.setText("-" + String.valueOf(progress));
                max.setText(String.valueOf(progress));
                Prog = String.valueOf(progress);


                if (String.valueOf(progress).equals("0")) {
                    min.setText("0");
                }

                //double range = seekBar.getProgress();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {


            }
        });

        progres2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {


                min2d.setText("-" + String.valueOf(progress));
                max2d.setText(String.valueOf(progress));
                Prog2d = String.valueOf(progress);


                if (String.valueOf(progress).equals("0")) {
                    min2d.setText("0");
                }

                //double range = seekBar.getProgress();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {


            }
        });

    }


    public void onClick(View view) {


        boolean valid=true;



            //boolean vvod=formula1.getText().toString().equals("");
            //sformula = formula1.getText().toString();
            if (formula1.getText().toString().equals("")) {
                Toast.makeText(getApplicationContext(), "Please enter the formula",
                        Toast.LENGTH_SHORT).show();
            }else{

                try {
                    Expression e1 = new ExpressionBuilder(formula1.getText().toString())
                            .variables("x", "y")
                            .build();
                    ValidationResult result = e1.validate(false);
                    valid = result.isValid();
                }catch (UnknownFunctionOrVariableException e){
                    valid = false;
                }

                if (valid == false){
                    Toast.makeText(getApplicationContext(), "Wrong formula",
                            Toast.LENGTH_SHORT).show();
                } else{
                    if (polar == true) {
                        Toast.makeText(getApplicationContext(), "Polar does not work, yet",
                                Toast.LENGTH_SHORT).show();
                    } else if (Prog.equals("0")) {
                        Toast.makeText(getApplicationContext(), "Please select range",
                                Toast.LENGTH_SHORT).show();
                    } else {


                        //Graph class1 = new Graph();
                        //class1.setId(sformula);
                        Toast.makeText(getApplicationContext(), "Loading...",
                                Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(MainActivity.this, Splash2.class);
                        intent.putExtra("formula", formula1.getText().toString());
                        intent.putExtra("min", min.getText().toString());
                        intent.putExtra("max", max.getText().toString());
                        startActivity(intent);
                    }
                }
        }
    }

    public void onClickadd(View view) {

        del.setVisibility(View.VISIBLE);
        int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 248, getResources().getDisplayMetrics());
        int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 57, getResources().getDisplayMetrics());
        int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
        int marginright = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getResources().getDisplayMetrics());
        LinearLayout.LayoutParams editadd = new LinearLayout.LayoutParams(width, height);
        editadd.gravity = Gravity.CENTER;
        editadd.bottomMargin = margin;
        editadd.rightMargin = 8;
        /*if(num==0){
            num=1;
        }*/
        if (num <= 2) {
            num++;
            EditText editnew = new EditText(this);
            editnew.setBackgroundResource(R.drawable.ic_edit);
            editnew.setGravity(Gravity.CENTER);
            editnew.setId(num);

            d2lay.addView(editnew, editadd);
            if (num == 1) {
                formul1 = (EditText) findViewById(num);
                formul1.setText("cos(x)");

            }
            if (num == 2) {
                formul2 = (EditText) findViewById(num);
                formul2.setText("x");

            }
            if (num == 3) {
                formul3 = (EditText) findViewById(num);
                formul3.setText("x^2");
                add.setVisibility(View.INVISIBLE);

            }


        }

    }

    public void onClickdell(View view) {


        if (num > 0) {
            d2lay.removeView(findViewById(num));
            add.setVisibility(View.VISIBLE);
            num--;
            if (num == 0) {
                del.setVisibility(View.INVISIBLE);
            }

        }

    }

    public void onClickscan(View view) {
        Intent intent = new Intent(MainActivity.this, ScanActivity.class);
        startActivityForResult(intent, CHOOSE_THIEF);
    }

    public void onClick2d(View view) {

        /*tnum = num;
        if(tnum==4){
            tnum=3;
        }*/

        boolean valid = true;
        boolean valid1 = true;
        /*try {
            Expression e1 = new ExpressionBuilder(formula2.getText().toString())
                    .variables("x", "y")
                    .build();
            // ValidationResult result = e1.validate();
            // valid = result.isValid();
        } catch (UnknownFunctionOrVariableException e) {
            valid = false;
        }*/

        if (formula2.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Please enter the formula",
                    Toast.LENGTH_SHORT).show();
        } else {
            if(polar2d==false) {
            try {

                    Expression e1 = new ExpressionBuilder(formula2.getText().toString())
                            .variables("x")
                            .build();
                    ValidationResult result = e1.validate(false);
                    valid = result.isValid();
                    //valid=e1.validate().isValid();

            } catch (UnknownFunctionOrVariableException e) {
                valid = false;
            }
            }else{

                try {

                    Expression e1 = new ExpressionBuilder(formula2.getText().toString())
                            .variables("f")
                            .build();
                    ValidationResult result = e1.validate(false);
                    valid = result.isValid();
                    //valid=e1.validate().isValid();

                } catch (UnknownFunctionOrVariableException e) {
                    valid = false;
                }

                //valid=e1.validate().isValid();
            }


            /*Expression e2 = new ExpressionBuilder(formula2.getText().toString())
                        .variables("x")
                        .build();
                ValidationResult result = e2.validate();
                valid1 = result.isValid();*/
                //valid1=e2.validate().isValid();


            if (valid == false ) {
                Toast.makeText(getApplicationContext(), "Wrong input in label 1",
                        Toast.LENGTH_SHORT).show();
            } else if (Prog2d.equals("0")) {
                Toast.makeText(getApplicationContext(), "Please select range",
                        Toast.LENGTH_SHORT).show();
            } else if (num > 0) {
                if (num == 1 && formul1.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Please enter the formula in 2",
                            Toast.LENGTH_SHORT).show();
                } else if(num ==1  && !formul1.getText().toString().equals("")){
                    try {
                        Expression e1 = new ExpressionBuilder(formul1.getText().toString())
                                .variables("x")
                                .build();
                        ValidationResult result = e1.validate(false);
                        valid = result.isValid();
                    } catch (UnknownFunctionOrVariableException e) {
                        valid = false;
                    }
                    if (valid == false) {
                        Toast.makeText(getApplicationContext(), "Wrong input in label 2",
                                Toast.LENGTH_SHORT).show();
                    }else{


                        Toast.makeText(getApplicationContext(), "Loading...",
                                Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(MainActivity.this, Graph2d.class);
                        intent.putExtra("formula", formula2.getText().toString());
                        intent.putExtra("min", min2d.getText().toString());
                        intent.putExtra("max", max2d.getText().toString());
                        intent.putExtra("num", num);
                        intent.putExtra("polar", polar2d);
                        if (num == 1) {
                            intent.putExtra("formula1", formul1.getText().toString());
                        }
                        if (num == 2) {
                            intent.putExtra("formula1", formul1.getText().toString());
                            intent.putExtra("formula2", formul2.getText().toString());
                        }
                        if (num == 3) {
                            intent.putExtra("formula1", formul1.getText().toString());
                            intent.putExtra("formula2", formul2.getText().toString());
                            intent.putExtra("formula3", formul3.getText().toString());
                        }
                        startActivity(intent);
                    }
                } else if (num == 2) {
                    if (formul1.getText().toString().equals("")) {
                        Toast.makeText(getApplicationContext(), "Please enter the formula in 2",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        try {
                            Expression e1 = new ExpressionBuilder(formul1.getText().toString())
                                    .variables("x")
                                    .build();
                            ValidationResult result = e1.validate(false);
                            valid = result.isValid();
                        } catch (UnknownFunctionOrVariableException e) {
                            valid = false;
                        }
                        if(valid==false){
                            Toast.makeText(getApplicationContext(), "Wrong input in label 2",
                                    Toast.LENGTH_SHORT).show();
                        }
                        else if (formul2.getText().toString().equals("")) {
                            Toast.makeText(getApplicationContext(), "Please enter the formula in 3",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            try {
                                Expression e1 = new ExpressionBuilder(formul2.getText().toString())
                                        .variables("x")
                                        .build();
                                ValidationResult result = e1.validate(false);
                                valid = result.isValid();
                            } catch (UnknownFunctionOrVariableException e) {
                                valid = false;
                            }
                            if(valid==false){
                                Toast.makeText(getApplicationContext(), "Wrong input in label 3",
                                        Toast.LENGTH_SHORT).show();
                            }else {

                                Toast.makeText(getApplicationContext(), "Loading...",
                                        Toast.LENGTH_SHORT).show();

                                Intent intent = new Intent(MainActivity.this, Graph2d.class);
                                intent.putExtra("formula", formula2.getText().toString());
                                intent.putExtra("min", min2d.getText().toString());
                                intent.putExtra("max", max2d.getText().toString());
                                intent.putExtra("num", num);
                                intent.putExtra("polar", polar2d);
                                if (num == 1) {
                                    intent.putExtra("formula1", formul1.getText().toString());
                                }
                                if (num == 2) {
                                    intent.putExtra("formula1", formul1.getText().toString());
                                    intent.putExtra("formula2", formul2.getText().toString());
                                }
                                if (num == 3) {
                                    intent.putExtra("formula1", formul1.getText().toString());
                                    intent.putExtra("formula2", formul2.getText().toString());
                                    intent.putExtra("formula3", formul3.getText().toString());
                                }
                                startActivity(intent);
                            }
                        }
                    }

                } else if (num == 3) {

                    if (formul1.getText().toString().equals("")) {
                        Toast.makeText(getApplicationContext(), "Please enter the formula in 2",
                                Toast.LENGTH_SHORT).show();
                    } else{
                        try {
                            Expression e1 = new ExpressionBuilder(formul1.getText().toString())
                                    .variables("x")
                                    .build();
                            ValidationResult result = e1.validate(false);
                            valid = result.isValid();
                        } catch (UnknownFunctionOrVariableException e) {
                            valid = false;
                        }
                        if(valid==false){
                            Toast.makeText(getApplicationContext(), "Wrong input in label 2",
                                    Toast.LENGTH_SHORT).show();
                        }
                        else if (formul2.getText().toString().equals("")) {
                            Toast.makeText(getApplicationContext(), "Please enter the formula in 3",
                                    Toast.LENGTH_SHORT).show();
                        } else{
                            try {
                                Expression e1 = new ExpressionBuilder(formul2.getText().toString())
                                        .variables("x")
                                        .build();
                                ValidationResult result = e1.validate(false);
                                valid = result.isValid();
                            } catch (UnknownFunctionOrVariableException e) {
                                valid = false;
                            }
                            if(valid==false){
                                Toast.makeText(getApplicationContext(), "Wrong input in label 3",
                                        Toast.LENGTH_SHORT).show();
                            } else if (formul3.getText().toString().equals("")) {
                                 Toast.makeText(getApplicationContext(), "Please enter the formula in 4",
                                         Toast.LENGTH_SHORT).show();
                            } else {
                                try {
                                    Expression e1 = new ExpressionBuilder(formul3.getText().toString())
                                            .variables("x")
                                            .build();
                                    ValidationResult result = e1.validate(false);
                                    valid = result.isValid();
                                } catch (UnknownFunctionOrVariableException e) {
                                    valid = false;
                                }
                                if(valid==false){
                                    Toast.makeText(getApplicationContext(), "Wrong input in label 4",
                                            Toast.LENGTH_SHORT).show();
                                } else{

                                    Toast.makeText(getApplicationContext(), "Loading...",
                                            Toast.LENGTH_SHORT).show();

                                    Intent intent = new Intent(MainActivity.this, Graph2d.class);
                                    intent.putExtra("formula", formula2.getText().toString());
                                    intent.putExtra("min", min2d.getText().toString());
                                    intent.putExtra("max", max2d.getText().toString());
                                    intent.putExtra("num", num);
                                    intent.putExtra("polar", polar2d);
                                    if (num == 1) {
                                        intent.putExtra("formula1", formul1.getText().toString());
                                    }
                                    if (num == 2) {
                                        intent.putExtra("formula1", formul1.getText().toString());
                                        intent.putExtra("formula2", formul2.getText().toString());
                                    }
                                    if (num == 3) {
                                        intent.putExtra("formula1", formul1.getText().toString());
                                        intent.putExtra("formula2", formul2.getText().toString());
                                        intent.putExtra("formula3", formul3.getText().toString());
                                    }
                                    startActivity(intent);
                                }
                            }

                        }
                    }

                } else {

                    Toast.makeText(getApplicationContext(), "Loading...",
                            Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(MainActivity.this, Graph2d.class);
                    intent.putExtra("formula", formula2.getText().toString());
                    intent.putExtra("min", min2d.getText().toString());
                    intent.putExtra("max", max2d.getText().toString());
                    intent.putExtra("num", num);
                    intent.putExtra("polar", polar2d);
                    if (num == 1) {
                        intent.putExtra("formula1", formul1.getText().toString());
                    }
                    if (num == 2) {
                        intent.putExtra("formula1", formul1.getText().toString());
                        intent.putExtra("formula2", formul2.getText().toString());
                    }
                    if (num == 3) {
                        intent.putExtra("formula1", formul1.getText().toString());
                        intent.putExtra("formula2", formul2.getText().toString());
                        intent.putExtra("formula3", formul3.getText().toString());
                    }
                    startActivity(intent);
                }
            } else {
                if (polar2d==true){
                    ad = new AlertDialog.Builder(context);
                    ad.setTitle("Attention!");  // заголовок
                    ad.setMessage("Polar coordinates are at an early stage of development.\n Do you want to continue?"); // сообщение
                    ad.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int arg1) {
                        /*Toast.makeText(context, "Вы сделали правильный выбор",
                                Toast.LENGTH_LONG).show();*/

                            Toast.makeText(getApplicationContext(), "Loading...",
                                    Toast.LENGTH_SHORT).show();

                            Intent intent = new Intent(MainActivity.this, Graph2d.class);
                            intent.putExtra("formula", formula2.getText().toString());
                            intent.putExtra("min", min2d.getText().toString());
                            intent.putExtra("max", max2d.getText().toString());
                            intent.putExtra("num", num);
                            intent.putExtra("polar", polar2d);
                            if (num == 1) {
                                intent.putExtra("formula1", formul1.getText().toString());
                            }
                            if (num == 2) {
                                intent.putExtra("formula1", formul1.getText().toString());
                                intent.putExtra("formula2", formul2.getText().toString());
                            }
                            if (num == 3) {
                                intent.putExtra("formula1", formul1.getText().toString());
                                intent.putExtra("formula2", formul2.getText().toString());
                                intent.putExtra("formula3", formul3.getText().toString());
                            }
                            startActivity(intent);

                        }
                    });
                    ad.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int arg1) {
                            /*Toast.makeText(context, "Возможно вы правы", Toast.LENGTH_LONG)
                                    .show();*/
                            contin=false;
                        }
                    });
                    ad.setCancelable(true);
                    ad.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        public void onCancel(DialogInterface dialog) {
                            /*Toast.makeText(context, "Вы ничего не выбрали",
                                    Toast.LENGTH_LONG).show();*/
                            contin=false;
                        }
                    });
                    ad.show();



                }else {
                    Intent intent = new Intent(MainActivity.this, Graph2d.class);
                    intent.putExtra("formula", formula2.getText().toString());
                    intent.putExtra("min", min2d.getText().toString());
                    intent.putExtra("max", max2d.getText().toString());
                    intent.putExtra("num", num);
                    intent.putExtra("polar", polar2d);
                    if (num == 1) {
                        intent.putExtra("formula1", formul1.getText().toString());
                    }
                    if (num == 2) {
                        intent.putExtra("formula1", formul1.getText().toString());
                        intent.putExtra("formula2", formul2.getText().toString());
                    }
                    if (num == 3) {
                        intent.putExtra("formula1", formul1.getText().toString());
                        intent.putExtra("formula2", formul2.getText().toString());
                        intent.putExtra("formula3", formul3.getText().toString());
                    }
                    startActivity(intent);
                }
            }

        }
    }

    public void onClickHelp(View view){
        Intent intent = new Intent(MainActivity.this, helpActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //TextView infoTextView = (TextView) findViewById(R.id.textViewInfo);

        if (requestCode == CHOOSE_THIEF) {
            if (resultCode == RESULT_OK) {
                String thiefname = data.getStringExtra(ScanActivity.THIEF);
                if (tab1 == 0) {
                    if(num==0) {
                        formula2.setText(thiefname);
                    }else{
                        if(qrnum==0){
                            formula2.setText(thiefname);
                            if(qrnum<num) {
                                qrnum++;
                            }else{
                                qrnum=0;
                            }
                        }else if(qrnum==1){
                            formul1.setText(thiefname);
                            if(qrnum<num) {
                                qrnum++;
                            }else{
                                qrnum=0;
                            }
                        }else if(qrnum==2){
                            formul2.setText(thiefname);
                            if(qrnum<num) {
                                qrnum++;
                            }else{
                                qrnum=0;
                            }
                        }else{
                            formul3.setText(thiefname);
                            qrnum=0;
                        }
                    }
                }
                if (tab1 == 1) {
                    formula1.setText(thiefname);
                }
            /*}else {
                infoTextView.setText(""); // стираем текст
            }*/

            }
        }
    }
}
