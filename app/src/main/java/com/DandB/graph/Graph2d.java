package com.DandB.graph;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Graph2d extends AppCompatActivity {

    LineChart lineChart;
    String F1;
    String F2;
    String F3;
    double i10=1;
    boolean obl=false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph2d);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        lineChart = (LineChart) findViewById(R.id.lineChart);

        List<Entry> entries = new ArrayList<>();
        ArrayList<Entry> entries1obl = new ArrayList<>();
        ArrayList<Entry> entries2obl = new ArrayList<>();
        ArrayList<Entry> entries1 = new ArrayList<>();
        ArrayList<Entry> entries2 = new ArrayList<>();
        ArrayList<Entry> entries3 = new ArrayList<>();

        double y;
        String F = getIntent().getExtras().getString("formula");
        String I = getIntent().getExtras().getString("min");
        String X = getIntent().getExtras().getString("max");
        int num = getIntent().getExtras().getInt("num");
        boolean polar = getIntent().getExtras().getBoolean("polar");
        double i = Double.parseDouble(String.valueOf(I));
        double imin=i;
        double x = Double.parseDouble(String.valueOf(X));


        if(num == 1){
            F1 = getIntent().getExtras().getString("formula1");
            //double i1=i;
            for (double j=imin; j <= x; j=j+0.01) {



                Expression e = new ExpressionBuilder(F1)
                        .variables("x")
                        .build()
                        .setVariable("x", j);
                y=e.evaluate();
                //i1 = i1 + 0.1;
                /*try{
                    e.setVariable("x", x).evaluate();
                }catch(Throwable cause) {
                    if (cause instanceof ArithmeticException && "Division by zero!".equals(cause.getMessage()))
                    {
                        break;
                    }

                }*/



                float xgraph = Float.parseFloat(String.valueOf(j));
                float ygraph = Float.parseFloat(String.valueOf(y));
                entries1.add(new Entry(xgraph, ygraph));
            }
        }
        if(num == 2){
            F1 = getIntent().getExtras().getString("formula1");
            F2 = getIntent().getExtras().getString("formula2");
            //double i1=i;
            //double i2=i;
            for (double j=imin; j <= x; j=j+0.01) {


                Expression e = new ExpressionBuilder(F1)
                        .variables("x")
                        .build()
                        .setVariable("x", j);
                y=e.evaluate();
                //i1 = i1 + 0.1;
                /*try{
                    e.setVariable("x", x).evaluate();
                }catch(Throwable cause) {
                    if (cause instanceof ArithmeticException && "Division by zero!".equals(cause.getMessage()))
                    {
                        break;
                    }

                }*/



                float xgraph = Float.parseFloat(String.valueOf(j));
                float ygraph = Float.parseFloat(String.valueOf(y));
                entries1.add(new Entry(xgraph, ygraph));
            }

            for (double j=imin; j <= x; j=j+0.01) {


                Expression e = new ExpressionBuilder(F2)
                        .variables("x")
                        .build()
                        .setVariable("x", j);
                y=e.evaluate();
               // i2 = i2 + 0.1;
                /*try{
                    e.setVariable("x", x).evaluate();
                }catch(Throwable cause) {
                    if (cause instanceof ArithmeticException && "Division by zero!".equals(cause.getMessage()))
                    {
                        break;
                    }

                }*/



                float xgraph = Float.parseFloat(String.valueOf(j));
                float ygraph = Float.parseFloat(String.valueOf(y));
                entries2.add(new Entry(xgraph, ygraph));
            }
        }
        if(num == 3){
            F1 = getIntent().getExtras().getString("formula1");
            F2 = getIntent().getExtras().getString("formula2");
            F3 = getIntent().getExtras().getString("formula3");

           /* double i1=i;
            double i2=i;
            double i3=i;*/

            for (double j=imin; j <= x; j=j+0.01) {


                Expression e = new ExpressionBuilder(F1)
                        .variables("x")
                        .build()
                        .setVariable("x", j);
                y=e.evaluate();
               // i1 = i + 0.1;
                /*try{
                    e.setVariable("x", x).evaluate();
                }catch(Throwable cause) {
                    if (cause instanceof ArithmeticException && "Division by zero!".equals(cause.getMessage()))
                    {
                        break;
                    }

                }*/



                float xgraph = Float.parseFloat(String.valueOf(j));
                float ygraph = Float.parseFloat(String.valueOf(y));
                entries1.add(new Entry(xgraph, ygraph));
            }

            for (double j=imin; j <= x; j=j+0.01) {


                Expression e = new ExpressionBuilder(F2)
                        .variables("x")
                        .build()
                        .setVariable("x", j);
                y=e.evaluate();
                //i2 = i2 + 0.1;
                /*try{
                    e.setVariable("x", x).evaluate();
                }catch(Throwable cause) {
                    if (cause instanceof ArithmeticException && "Division by zero!".equals(cause.getMessage()))
                    {
                        break;
                    }

                }*/



                float xgraph = Float.parseFloat(String.valueOf(j));
                float ygraph = Float.parseFloat(String.valueOf(y));
                entries2.add(new Entry(xgraph, ygraph));
            }

            for (double j=imin; j <= x; j=j+0.01) {


                Expression e = new ExpressionBuilder(F3)
                        .variables("x")
                        .build()
                        .setVariable("x", j);
                y=e.evaluate();
                //i3 = i3 + 0.1;
                /*try{
                    e.setVariable("x", x).evaluate();
                }catch(Throwable cause) {
                    if (cause instanceof ArithmeticException && "Division by zero!".equals(cause.getMessage()))
                    {
                        break;
                    }

                }*/



                float xgraph = Float.parseFloat(String.valueOf(j));
                float ygraph = Float.parseFloat(String.valueOf(y));
                entries3.add(new Entry(xgraph, ygraph));
            }
        }



        //int numDataPoints = 1000;
        //

       // double iobl=i;
        ArrayList<ILineDataSet> lineDataSets = new ArrayList<>();
        if(polar==true){
            for (double grad=-2; grad <= 2; grad=grad+0.01) {

                double r;

                double xrad;
                double yrad;

                double rad;
                //rad = grad*(3.14/180);
                Expression e = new ExpressionBuilder(F)
                        .variables("f")
                        .build()
                        .setVariable("f", grad);
                r = e.evaluate();

                xrad=r*Math.cos(grad);
                yrad=r*Math.sin(grad);

                //float xgraph = Float.parseFloat(String.valueOf(xrad));
                //float ygraph = Float.parseFloat(String.valueOf(yrad));
                entries.add(new Entry((float)xrad, (float)yrad));
            }

        }else {
            for (double j = imin; j <= x; j = j + 0.01) {

                // DecimalFormat df = new DecimalFormat("#.#");

                j = Math.rint(100.0 * j) / 100.0;
                //iobl = iobl + 0.1;


                try {
                    Expression e1 = new ExpressionBuilder(F)
                            .variables("x")
                            .build()
                            .setVariable("x", j);
                    e1.evaluate();
                } catch (ArithmeticException e1) {

                    i10 = j;
                    obl = true;
                    break;


                }
            }

            //obl=true;
            if (obl == true) {
                for (double j = imin; j < i10 - 0.1; j = j + 0.01) {
                    Expression e = new ExpressionBuilder(F)
                            .variables("x")
                            .build()
                            .setVariable("x", j);
                    y = e.evaluate();
                    float xgraph = Float.parseFloat(String.valueOf(j));
                    float ygraph = Float.parseFloat(String.valueOf(y));
                    entries1obl.add(new Entry(xgraph, ygraph));
                }

                LineDataSet lineDataSet1obl = new LineDataSet(entries1obl, F);
                lineDataSet1obl.setDrawCircles(false);
                lineDataSet1obl.setColor(Color.BLUE);

                lineDataSets.add(lineDataSet1obl);

                for (double j = i10 + 0.1; j <= x; j = j + 0.01) {
                    Expression e = new ExpressionBuilder(F)
                            .variables("x")
                            .build()
                            .setVariable("x", j);
                    y = e.evaluate();
                    float xgraph = Float.parseFloat(String.valueOf(j));
                    float ygraph = Float.parseFloat(String.valueOf(y));
                    entries2obl.add(new Entry(xgraph, ygraph));
                }
                LineDataSet lineDataSet2obl = new LineDataSet(entries2obl, "");
                lineDataSet2obl.setDrawCircles(false);
                lineDataSet2obl.setColor(Color.BLUE);

                lineDataSets.add(lineDataSet2obl);
            } else {
                for (double j = imin; j <= x; j=j+0.01) {


                /*iobl = iobl + 0.1;

                try {
                    Expression e1 = new ExpressionBuilder(F)
                            .variables("x")
                            .build()
                            .setVariable("x", iobl);
                    e1.evaluate();
                } catch (ArithmeticException e4) {

                    i10 = iobl;
                    obl = true;
                    continue;


                }*/
                   // i = i + 0.1;
                    Expression e = new ExpressionBuilder(F)
                            .variables("x")
                            .build()
                            .setVariable("x", j);
                    y = e.evaluate();
                    // i = i + 0.1;
            /*try{
                e.setVariable("x", x).evaluate();
            }catch(Throwable cause) {
                if (cause instanceof ArithmeticException && "Division by zero!".equals(cause.getMessage()))
                {
                    break;
                }

            }*/


                    float xgraph = Float.parseFloat(String.valueOf(j));
                    float ygraph = Float.parseFloat(String.valueOf(y));
                    entries.add(new Entry(xgraph, ygraph));
                }
            }
        }

        if (num==0){

            if (obl == false){
            LineDataSet lineDataSet1 = new LineDataSet(entries,F);
            lineDataSet1.setDrawCircles(false);
            lineDataSet1.setColor(Color.BLUE);

            lineDataSets.add(lineDataSet1);
            }

            lineChart.setData(new  LineData(lineDataSets));
            lineChart.invalidate();
            lineChart.animateX(4000);

        }
        if (num==1){
            if (obl == false) {
                LineDataSet lineDataSet1 = new LineDataSet(entries, F);
                lineDataSet1.setDrawCircles(false);
                lineDataSet1.setColor(Color.BLUE);

                lineDataSets.add(lineDataSet1);
            }
            LineDataSet lineDataSet2 = new LineDataSet(entries1,F1);
            lineDataSet2.setDrawCircles(false);
            lineDataSet2.setColor(Color.RED);


            lineDataSets.add(lineDataSet2);

            lineChart.setData(new  LineData(lineDataSets));
            lineChart.invalidate();
            lineChart.animateX(4000);
        }
        if (num==2){
            if (obl == false) {
                LineDataSet lineDataSet1 = new LineDataSet(entries, F);
                lineDataSet1.setDrawCircles(false);
                lineDataSet1.setColor(Color.BLUE);

                lineDataSets.add(lineDataSet1);
            }

            LineDataSet lineDataSet2 = new LineDataSet(entries1,F1);
            lineDataSet2.setDrawCircles(false);
            lineDataSet2.setColor(Color.RED);

            LineDataSet lineDataSet3 = new LineDataSet(entries2,F2);
            lineDataSet3.setDrawCircles(false);
            lineDataSet3.setColor(Color.GREEN);


            lineDataSets.add(lineDataSet2);
            lineDataSets.add(lineDataSet3);

            lineChart.setData(new  LineData(lineDataSets));
            lineChart.invalidate();
            lineChart.animateX(4000);
        }
        if (num==3){
            if (obl == false) {
                LineDataSet lineDataSet1 = new LineDataSet(entries, F);
                lineDataSet1.setDrawCircles(false);
                lineDataSet1.setColor(Color.BLUE);

                lineDataSets.add(lineDataSet1);
            }

            LineDataSet lineDataSet2 = new LineDataSet(entries1,F1);
            lineDataSet2.setDrawCircles(false);
            lineDataSet2.setColor(Color.RED);

            LineDataSet lineDataSet3 = new LineDataSet(entries2,F2);
            lineDataSet3.setDrawCircles(false);
            lineDataSet3.setColor(Color.GREEN);

            LineDataSet lineDataSet4 = new LineDataSet(entries3,F3);
            lineDataSet4.setDrawCircles(false);
            lineDataSet4.setColor(Color.YELLOW);


            lineDataSets.add(lineDataSet2);
            lineDataSets.add(lineDataSet3);
            lineDataSets.add(lineDataSet4);

            lineChart.setData(new  LineData(lineDataSets));
            lineChart.invalidate();
            lineChart.animateX(2000);
        }


    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                /*Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);*/
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onClickPhoto2d(View view) {
        //Date now = new Date();
        //android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

       // String mPath = Environment.getExternalStorageDirectory().toString() + "/" + now + ".jpg";

        //File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "savedBitmap.jpg");
        Bitmap viewBmp = Bitmap.createBitmap(lineChart.getWidth(), lineChart.getHeight(), Bitmap.Config.ARGB_8888);
        viewBmp.setDensity(lineChart.getResources().getDisplayMetrics().densityDpi);
        Canvas canvas = new Canvas(viewBmp);
        lineChart.draw(canvas);

       /* lineChart.setDrawingCacheEnabled(true);
        lineChart.buildDrawingCache();
        Bitmap viewBmp = Bitmap.createBitmap(lineChart.getDrawingCache());
        lineChart.setDrawingCacheEnabled(false);*/

        //File imageFile = new File(mPath);

        try {
            File dest = new File(Environment.getExternalStorageDirectory() + "/"+"GraphScreenshot");
            dest.mkdirs();
            dest = new File(Environment.getExternalStorageDirectory() + "/"+"GraphScreenshot/"+System.currentTimeMillis()/1000+".jpg");
            FileOutputStream out = new FileOutputStream(dest);
            viewBmp.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
           /* MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
            MediaScannerConnection.scanFile(getActivity(),
                    new String[]{dest.toString()}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {
                            Log.i("ExternalStorage", "Scanned " + path + ":");
                            Log.i("ExternalStorage", "-> uri=" + uri);
                        }
                    });*/
            Toast.makeText(getApplicationContext(), "Saved in "+Environment.getExternalStorageDirectory() + "/"+"GraphScreenshot",
                    Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error, please try again",
                    Toast.LENGTH_SHORT).show();
            Log.e("MyLog", e.toString());
        }
    }

}
