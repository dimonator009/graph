package com.DandB.graph;

import android.app.ActionBar;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ConfigurationInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.opengl.GLES20;
import android.opengl.GLException;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.view.View.OnTouchListener;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.opengles.GL10;

import static android.opengl.GLES10.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.GL_DEPTH_BUFFER_BIT;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glDrawArrays;
import static java.lang.Integer.valueOf;


public class Graph extends AppCompatActivity implements OnTouchListener {

    float x1,x2;
    float xx, xxx;
    float y1, y2;
    float yy, yyy;
    int Rad=1;
    boolean pov=false;
    TextView xtxt, ytxt, ztxt;
    float centr = 0;
    SeekBar rotate;
    String xmin, zmin, zmax="0", xmax="0";
    int height1;
    boolean flag1 = false;
    ScrollView scrollView;


    //public String s1formula;

    //public  MainActivity getformula = new MainActivity();

    //String s1formula = getformula.getsformula();

    /*String id;



    public void setId(String id) {
        this.id = id;
    }*/

    //Intent intent = getIntent();
    //String s1formula = intent.getStringExtra("formula");




    private GLSurfaceView glSurfaceView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        //MainActivity getformula = new MainActivity();
        //s1formula = getformula.getsformula();
        //s1formula = getIntent().getStringExtra("formula");
        //s1formula = "x^2+y^2";
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        final int height = size.y;
        height1=(int)height*(70)/100;


        if (!supportES2()) {
            Toast.makeText(this, "OpenGL ES 2.0 is not supported", Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        scrollView =new ScrollView(this);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        ViewGroup.LayoutParams linLayoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        //scrollView.addView(linearLayout);
        setContentView(linearLayout, linLayoutParams);
        glSurfaceView = new GLSurfaceView(this);
        //glSurfaceView = (GLSurfaceView) findViewById(R.id.glSurfaceView);
        glSurfaceView.setEGLContextClientVersion(2);
        glSurfaceView.setRenderer(new OpenGLRenderer1(this));
        glSurfaceView.setOnTouchListener(this);

        final LinearLayout.LayoutParams glview = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height1);
        glSurfaceView.setLayoutParams(glview);

        linearLayout.addView(glSurfaceView);

        int margintop = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
        int razheight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 44, getResources().getDisplayMetrics());
        LinearLayout.LayoutParams raz = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, razheight);
        //raz.bottomMargin = marginbottom;
        final ImageButton razdel = new ImageButton(this);
        razdel.setBackgroundResource(R.drawable.up);
        razdel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Toast.makeText(getApplicationContext(), "Polar does not work, yet",
                       // Toast.LENGTH_SHORT).show();
                if(flag1 == false) {
                    for (int i = 0; i <= 50; i++) {
                        height1 = (int) height * (70 - i) / 100;
                        glview.height = height1;
                        glSurfaceView.setLayoutParams(glview);
                    }
                    razdel.setBackgroundResource(R.drawable.down);
                    //scrollView.smoothScrollTo(0,0);
                    flag1=true;
                }else{
                    for (int i = 0; i <= 50; i++) {
                        height1 = (int) height * (20 + i) / 100;
                        glview.height = height1;
                        glSurfaceView.setLayoutParams(glview);
                    }
                    razdel.setBackgroundResource(R.drawable.up);
                    flag1=false;
                    scrollView.smoothScrollTo(0,0);
                }

            }
        });
        linearLayout.addView(razdel, raz);

        linearLayout.addView(scrollView);

        LinearLayout linearLayout1 = new LinearLayout(this);
        linearLayout1.setOrientation(LinearLayout.VERTICAL);
        scrollView.addView(linearLayout1);


        LinearLayout.LayoutParams centr = new LinearLayout.LayoutParams((int)width*70/100, ViewGroup.LayoutParams.WRAP_CONTENT);
        centr.gravity = Gravity.CENTER;
        centr.topMargin=margintop;
        centr.bottomMargin = margintop;

        int whzum = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getResources().getDisplayMetrics());
        LinearLayout.LayoutParams centr2 = new LinearLayout.LayoutParams(whzum, whzum);
        centr2.gravity = Gravity.CENTER;
        centr2.topMargin=margintop;
        centr2.bottomMargin = margintop;

        LinearLayout linearLayout2 = new LinearLayout(this);
        linearLayout2.setOrientation(LinearLayout.HORIZONTAL);
        ImageView zumplus = new ImageView(this);
        zumplus.setBackgroundResource(R.drawable.ic_zumplus);
        linearLayout2.addView(zumplus, centr2);



        rotate = new SeekBar(this);
        rotate.setMax(100);
        rotate.setProgress(1);
        linearLayout2.addView(rotate, centr);

        ImageView zumminus = new ImageView(this);
        zumminus.setBackgroundResource(R.drawable.ic_zumminus);
        linearLayout2.addView(zumminus, centr2);

        LinearLayout.LayoutParams centr3 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        centr3.gravity = Gravity.CENTER;
        linearLayout1.addView(linearLayout2, centr3);

        /*ImageButton photobutton = new ImageButton(this);
        photobutton.setBackgroundResource(R.drawable.photo);
        photobutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EGL10 egl = (EGL10) EGLContext.getEGL();
                GL10 gl = (GL10)egl.eglGetCurrentContext().getGL();
                //Bitmap viewBmp = Bitmap.createBitmap(glSurfaceView.getWidth(), glSurfaceView.getHeight(), Bitmap.Config.ARGB_8888);
                Bitmap viewBmp = createBitmapFromGLSurface(0, 0, glSurfaceView.getWidth(), glSurfaceView.getHeight(),gl);
                //viewBmp.setDensity(glSurfaceView.getResources().getDisplayMetrics().densityDpi);
                //Canvas canvas = new Canvas(viewBmp);
                //glSurfaceView.draw(canvas);

                try {
                    File dest = new File(Environment.getExternalStorageDirectory() + "/"+"GraphScreenshot");
                    dest.mkdirs();
                    dest = new File(Environment.getExternalStorageDirectory() + "/"+"GraphScreenshot/"+System.currentTimeMillis()/1000+".jpg");
                    FileOutputStream out = new FileOutputStream(dest);
                    viewBmp.compress(Bitmap.CompressFormat.JPEG, 90, out);
                    out.flush();
                    out.close();
           /* MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
            MediaScannerConnection.scanFile(getActivity(),
                    new String[]{dest.toString()}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {
                            Log.i("ExternalStorage", "Scanned " + path + ":");
                            Log.i("ExternalStorage", "-> uri=" + uri);
                        }
                    });*/
                    /*Toast.makeText(getApplicationContext(), "Save in "+Environment.getExternalStorageDirectory() + "/"+"GraphScreenshot",
                            Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Error, try again",
                            Toast.LENGTH_SHORT).show();
                    Log.e("MyLog", e.toString());
                }
            }
        });
        int heightbutton = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 45, getResources().getDisplayMetrics());
        int weightbutton = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 120, getResources().getDisplayMetrics());
        LinearLayout.LayoutParams centr1ph = new LinearLayout.LayoutParams(weightbutton, heightbutton);
        centr1ph.gravity = Gravity.CENTER;
        centr1ph.topMargin=margintop;
        centr1ph.bottomMargin = margintop;
        linearLayout1.addView(photobutton, centr1ph);*/


        int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
        LinearLayout.LayoutParams centr1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        centr1.gravity = Gravity.CENTER;
        centr1.bottomMargin = margin;
        centr1.leftMargin = margin;
        centr1.rightMargin = margin;



        xtxt = new TextView(this);
        xtxt.setGravity(Gravity.CENTER);
        xtxt.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
        xtxt.setBackgroundResource(R.drawable.ic_edit);
        xtxt.setText("X :\nminimum = "+xmin+"\nmaximum = "+xmax);
        linearLayout1.addView(xtxt, centr1);

        ytxt = new TextView(this);
        ytxt.setGravity(Gravity.CENTER);
        ytxt.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
        ytxt.setBackgroundResource(R.drawable.ic_edit);
        ytxt.setText("Y :\nminimum = "+xmin+"\nmaximum = "+xmax);
        linearLayout1.addView(ytxt, centr1);

        ztxt = new TextView(this);
        ztxt.setGravity(Gravity.CENTER);
        ztxt.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
        ztxt.setBackgroundResource(R.drawable.ic_edit);
        ztxt.setText("Z :\nminimum = "+zmin+"\nmaximum = "+zmax);
        linearLayout1.addView(ztxt, centr1);


        rotate.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {



                Rad = valueOf(progress)+1;

                //double range = seekBar.getProgress();
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {




            }
        });

        //linearLayout.addView(activity_main);
        //glSurfaceView.setOnTouchListener(this);
        //setContentView(glSurfaceView);
        //setContentView(R.layout.activity_graph);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                /*Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);*/
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean onTouch(View v, MotionEvent event) {

        //x = event.getX();
        //y = event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                pov=true;// нажатие
                x1 = event.getX();
                y1 = event.getY();
                break;
            case MotionEvent.ACTION_MOVE: // движение
                x2 = event.getX();
                y2 = event.getY();
                break;

        }
        xx=x1-x2;
        yy=y1-y2;
        //xmm.setText(String.valueOf(xx)+" "+String.valueOf(yy));
        xxx+=xx;
        yyy+=yy;


        return true;
    }
    /*public float getx() {
        return x1-x2;
    }*/

    /*public float gety() {
        return y1-y2;
    }*/

    @Override
    protected void onPause() {
        super.onPause();
        glSurfaceView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        glSurfaceView.onResume();
    }

    /*public String getS1formula() {
        return s1formula;
    }*/

    private boolean supportES2() {
        ActivityManager activityManager =
                (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        ConfigurationInfo configurationInfo = activityManager.getDeviceConfigurationInfo();
        return (configurationInfo.reqGlEsVersion >= 0x20000);
    }

    /*package com.DandB.graph;

import android.content.Context;
import android.content.Intent;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;*/

    /**
     * Created by dmitriy on 03.06.18.
     */
    public class OpenGLRenderer1 implements GLSurfaceView.Renderer{
        // интерфейс GLSurfaceView.Renderer содержит
// три метода onDrawFrame, onSurfaceChanged, onSurfaceCreated
// которые должны быть переопределены
// текущий контекст
        private Context context;


        //public  Graph formula = new Graph();


        //private String id;

        //Intent intent = getIntent();

        //Graph y1 = new Graph();
        //координаты камеры
        private float xСamera, yCamera, zCamera;
        //координаты источника света
        private float xLightPosition, yLightPosition, zLightPosition;
        //матрицы
        private float[] modelMatrix;
        private float[] viewMatrix;
        private float[] modelViewMatrix;
        private float[] projectionMatrix;
        private float[] modelViewProjectionMatrix;
        //размеры сетки
        public  String min = getIntent().getExtras().getString("min");
        public String max = getIntent().getExtras().getString("max");
        private int imax=(Math.abs(Integer.parseInt(String.valueOf(min)))+Math.abs(Integer.parseInt(String.valueOf(max))));
        private int jmax=imax;
        //private  float r = imax+20;
        private  float centrx = (Integer.parseInt(String.valueOf(max))-Integer.parseInt(String.valueOf(min)))/2 + Integer.parseInt(String.valueOf(min));
        private  float centrz = centrx;
        private  float ymin, ymax;

        float ratio ;
        float k;
        float left ;
        float right;
        float bottom;
        float top;
        float near ;
        float far;
        //
        //размер индексного массива
        private int sizeindex;
        //начальная координата x
        private float x0=Integer.parseInt(String.valueOf(min));
        //начальная координата z
        private float z0=Integer.parseInt(String.valueOf(min));
        //шаг сетки по оси x
        private float dx=0.1f;
        //шаг сетки по оси z
        private float dz=0.1f;
        // массив для хранения координаты x
        private float [] x;
        // массив для хранения координаты y
        private float [][] y;
        //массив для хранения координаты z
        private float [] z;
        //массив для хранения координат вершин для записи в буфер
        private float [] vertex;
        //массивы для хранения координат вектора нормали
        private float [][] normalX;
        private float [][] normalY;
        private float [][] normalZ;
        //массив для хранения координат вектора нормали для записи в буфер
        private float [] normal;
        //буферы для координат вершин и нормалей
        private FloatBuffer vertexBuffer, normalBuffer;
        //буфер индексов
        private ShortBuffer indexBuffer;
        float j=0;
        //шейдерный объект
        private Shader mShader;
        //------------------------------------------------------------------------------------------
//конструктор
        public OpenGLRenderer1(Context context) {

           // xtxt.setText("X : minimum = "+max+"maximum = "+max);
            //ytxt.setText("X : minimum = "+min+"maximum = "+max);

            xmin=min;
            xmax=max;
            //Graph formula = new Graph();
            // запомним контекст
            // он нам понадобится в будущем для загрузки текстур
            this.context=context;
            //координаты точечного источника света
            xLightPosition=5f;
            yLightPosition=5f;
            zLightPosition=5f;
            //матрицы
            modelMatrix=new float[16];
            viewMatrix=new float[16];
            modelViewMatrix=new float[16];
            projectionMatrix=new float[16];
            modelViewProjectionMatrix=new float[16];
            //мы не будем двигать объекты
            //поэтому сбрасываем модельную матрицу на единичную
            Matrix.setIdentityM(modelMatrix, 0);
            //координаты камеры
            xСamera=0;
            yCamera=0;
            zCamera=0;
        /*xСamera=0.3f;
        yCamera=1.7f;
        zCamera=1.5f;*/
            if(imax <=8){
                dx=0.1f;
                dz=0.1f;
                imax = imax*10;
                jmax = imax;

            }else if(imax > 8 && imax <= 30){
                dx=0.25f;
                dz=0.25f;
                imax = imax*4;
                jmax = imax;

            }else if(imax > 30 &&  imax <= 60){
                dx=0.5f;
                dz=0.5f;
                imax = imax*2;
                jmax = imax;
            }else if(imax > 60){
                dx=1f;
                dz=1f;
                imax = imax*1;
                jmax = imax;
            }
            //пусть камера смотрит на начало координат
            //и верх у камеры будет вдоль оси Y
            //зная координаты камеры получаем матрицу вида
            Matrix.setLookAtM(viewMatrix, 0, xСamera, yCamera, zCamera, 10, 0, 0, 0, 1, 0);
            // умножая матрицу вида на матрицу модели
            // получаем матрицу модели-вида
            Matrix.multiplyMM(modelViewMatrix, 0, viewMatrix, 0, modelMatrix, 0);
            // создаем массивы
            x=new float [imax+1];
            z=new float [jmax+1];
            y=new float [jmax+1][imax+1];
            vertex=new float[(jmax+1)*(imax+1)*3];
            normalX=new float[jmax+1][imax+1];
            normalY=new float[jmax+1][imax+1];
            normalZ=new float[jmax+1][imax+1];
            normal=new float[(jmax+1)*(imax+1)*3];
            //заполним массивы x и z координатами сетки

            for (int i=0; i<=imax; i++){
                x[i]=x0+i*dx;
            }
            for (int j=0; j<=jmax; j++){
                z[j]=z0+j*dz;
            }
            //создадим буфер для хранения координат вершин
            // он заполняется в методе getVertex()
            ByteBuffer vb = ByteBuffer.allocateDirect((jmax+1)*(imax+1)*3*4);
            vb.order(ByteOrder.nativeOrder());
            vertexBuffer = vb.asFloatBuffer();
            vertexBuffer.position(0);
            //создадим буфер для хранения координат векторов нормалей
            // он заполняется в методе getNormal()
            ByteBuffer nb = ByteBuffer.allocateDirect((jmax+1)*(imax+1)*3*4);
            nb.order(ByteOrder.nativeOrder());
            normalBuffer = nb.asFloatBuffer();
            normalBuffer.position(0);
            //индексы
            // временный массив индексов
            short[] index;
            // 2*(imax+1) - количество индексов в ленте
            // jmax - количество лент
            // (jmax-1) - добавленные индексы для связки лент
            sizeindex=2*(imax+1)*jmax + (jmax-1);
            index = new short[sizeindex];
            // расчет массива индексов для буфера
            int k=0;
            int j=0;
            while (j < jmax) {
                // лента слева направо
                for (int i = 0; i <= imax; i++) {
                    index[k] = chain(j,i);
                    k++;
                    index[k] = chain(j+1,i);
                    k++;
                }
                if (j < jmax-1){
                    // вставим хвостовой индекс для связки
                    index[k] = chain(j+1,imax);
                    k++;
                }
                // переводим ряд
                j++;

                // проверяем достижение конца
                if (j < jmax){
                    // лента справа налево
                    for (int i = imax; i >= 0; i--) {
                        index[k] = chain(j,i);
                        k++;
                        index[k] = chain(j+1,i);
                        k++;
                    }
                    if (j < jmax-1){
                        // вставим хвостовой индекс для связки
                        index[k] = chain(j+1,0);
                        k++;
                    }
                    // переводим ряд
                    j++;
                }
            }
            // буфер индексов - тип short содержит 2 байта
            ByteBuffer bi = ByteBuffer.allocateDirect(sizeindex * 2);
            bi.order(ByteOrder.nativeOrder());
            indexBuffer = bi.asShortBuffer();
            // заполняем буфер индексов
            indexBuffer.put(index);
            indexBuffer.position(0);
            // уничтожаем временный массив индексов,
            // т.к. в дальнейшем нужен только буфер индексов
            index = null;
            //начальное заполнение буферов вершин и нормалей
            getVertex();
            getNormal();
        }//конец конструктора
        //------------------------------------------------------------------------------------------
// вспомогательная функция
// возвращает порядковый номер вершины по известным j и i
        private short chain(int j, int i){
            return (short) (i+j*(imax+1));
        }
        //------------------------------------------------------------------------------------------
//метод выполняет расчет координат вершин
        private void getVertex(){
            // заполним массив Y значениями функции
            String formul = getIntent().getExtras().getString("formula");
            Expression e = new ExpressionBuilder(formul)
                    .variables("x", "y")
                    .build()
                    .setVariable("x", x[0])
                    .setVariable("y", z[0]);
            ymin = (float)e.evaluate();
            ymax = (float)e.evaluate();
            for (int j=0; j<=jmax; j++){
                for (int i=0; i<=imax; i++){


                    // zCamera=1.5f;



                    ///double time=System.currentTimeMillis();
                    //xСamera=0.005f*(float)time;
                    //yCamera=0.005f*(float)time;
                    //y[j][i]=(float)Math.exp(-3*(x[i]*x[i]+z[j]*z[j]));
                    //Graph formula = new Graph();
                    //String formul= formula.getS1formula();
                    //String formul = getIntent().getExtras().getString("formula");

                    try{
                        Expression e2 = new ExpressionBuilder(formul)
                                .variables("x", "y")
                                .build()
                                .setVariable("x", x[i])
                                .setVariable("y", z[j]);
                        e2.evaluate();
                    }catch(ArithmeticException e3) {

                        continue;

                    }
                    Expression e1 = new ExpressionBuilder(formul)
                            .variables("x", "y")
                            .build()
                            .setVariable("x", x[i])
                            .setVariable("y", z[j]);
                    y[j][i] = (float)e1.evaluate();
                    if (y[j][i] > ymax){
                        ymax = y[j][i];
                    }
                    if (y[j][i] < ymin){
                        ymin = y[j][i];
                    }
                    //y[j][i]=(x[i]*x[i]+z[j]*z[j]);
                    // y[j][i]=0.2f*(float)Math.cos(0.005*time+5*(z[j]+x[i]));
                }

                zmin=String.valueOf(ymin);
                zmax=String.valueOf(ymax);
                //ztxt.setText("X : minimum = "+zmin+"maximum = "+zmax);


                //ztxt.setText("X : minimum = "+String.format+"maximum = "+String.valueOf(ymax));
            }
            // заполним массив координат vertex
            int k=0;
            for (int j=0; j<=jmax; j++){
                for (int i=0; i<=imax; i++){
                    vertex[k]=x[i];
                    k++;
                    vertex[k]=y[j][i];
                    k++;
                    vertex[k]=z[j];
                    k++;
                }
            }
            //перепишем координаты вершин из массива vertex в буфер координат вершин
            vertexBuffer.put(vertex);
            vertexBuffer.position(0);
        }//конец метода
        //------------------------------------------------------------------------------------------
//метод выполняет расчет векторов нормалей
//по известным координатам вершин
        private void getNormal(){
            for (int j=0; j<jmax; j++){
                for (int i=0; i<imax; i++){
                    normalX [j] [i] = - ( y [j] [i+1] - y [j] [i] ) * dz;
                    normalY [j] [i] = dx * dz;
                    normalZ [j] [i] = - dx * ( y [j+1] [i] - y [j] [i] );
                }
            }
            //нормаль для i=imax
            for (int j=0; j<jmax; j++){
                normalX [j] [imax] = ( y [ j ] [ imax -1] - y [ j ] [ imax] ) * dz;
                normalY [j] [imax] = dx * dz;
                normalZ [j] [imax] = - dx * ( y [ j+1 ] [ imax] - y [ j ] [ imax ] );
            }
            //нормаль для j=jmax
            for (int i=0; i<imax; i++){
                normalX [jmax] [ i ] = - ( y [ jmax ] [ i+1 ] - y [ jmax ] [ i ] ) * dz;
                normalY [jmax] [ i ] = dx * dz;
                normalZ [jmax] [ i ] = dx * ( y [ jmax-1 ] [ i ] - y [ jmax ] [ i ] );
            }
            //нормаль для i=imax и j=jmax
            normalX [jmax] [ imax ]= (y [ jmax] [ imax-1] - y [ jmax] [imax]) * dz;
            normalY [jmax] [ imax ] = dx * dz;
            normalZ [jmax] [ imax ] = dx * (y [jmax-1] [imax] - y[jmax ] [imax]);
            //переписываем координаты вектора нормали в одномерный массив normal
            int k=0;
            for (int j=0; j<=jmax; j++){
                for (int i=0; i<=imax; i++){
                    normal[k]=normalX[j][i];
                    k++;
                    normal[k]=normalY[j][i];
                    k++;
                    normal[k]=normalZ[j][i];
                    k++;
                }
            }
            //отправляем одномерный массив normal в буфер
            normalBuffer.put(normal);
            normalBuffer.position(0);
        } // конец метода
        //------------------------------------------------------------------------------------------
//метод, который срабатывает при изменении размеров экрана
//в нем мы получим матрицу проекции и матрицу модели-вида-проекции



        public void onSurfaceChanged(GL10 unused, int width, int height) {
            // устанавливаем glViewport
            GLES20.glViewport(0, 0, width, height);
            ratio = (float) width / height;
            k=0.055f;
            left = -k*ratio;
            right = k*ratio;
            bottom = -k;
            top = k;
            near = 0.1f;
            far = Math.abs(Rad*4);
            // получаем матрицу проекции
            Matrix.frustumM(projectionMatrix, 0, left, right, bottom, top, near, far);
            // матрица проекции изменилась,
            // поэтому нужно пересчитать матрицу модели-вида-проекции
            Matrix.multiplyMM( modelViewProjectionMatrix, 0, projectionMatrix, 0, modelViewMatrix, 0);
        } //конец метода
        //------------------------------------------------------------------------------------------
//метод, который срабатывает при создании экрана
//здесь мы создаем шейдерный объект
        public void onSurfaceCreated(GL10 unused, EGLConfig config) {
            //включаем тест глубины
            GLES20.glClearColor(1.0f, 1.0f, 1.0f, 0.0f);

            centr = (ymax-ymin)/2.0f+ymin;


            rotate.setMax((int)(Math.abs(ymax)+Math.abs(ymax))*2+imax);

            //xmm.setText(String.valueOf(centr)+ ""+String.valueOf(ymax)+""+String.valueOf(ymin));


            GLES20.glEnable(GLES20.GL_DEPTH_TEST);
            //включаем отсечение невидимых граней
            //GLES20.glEnable(GLES20.GL_CULL_FACE);
            //GLES20.glEnable(GLES20.GL);
            //включаем сглаживание текстур, это пригодится в будущем
            GLES20.glHint(GLES20.GL_GENERATE_MIPMAP_HINT, GLES20.GL_NICEST);
            //простые шейдеры для освещения
            String vertexShaderCode=
                    "uniform mat4 u_modelViewProjectionMatrix;"+
                            "attribute vec3 a_vertex;"+
                            "attribute vec3 a_normal;"+
                            "varying vec3 v_vertex;"+
                            "varying vec3 v_normal;"+
                            "void main() {"+
                            "v_vertex=a_vertex;"+
                            "vec3 n_normal=normalize(a_normal);"+
                            "v_normal=n_normal;"+
                            "gl_Position = u_modelViewProjectionMatrix * vec4(a_vertex,1.0);"+
                            "}";

            String fragmentShaderCode=
                    "precision mediump float;"+
                            "uniform vec3 u_camera;"+
                            "uniform vec3 u_lightPosition;"+
                            // это новая строчка для нового источника на всякий весь шейдер вставь
                            "uniform vec3 u_lightPosition1;"+
                            "varying vec3 v_vertex;"+
                            "varying vec3 v_normal;"+
                            "void main() {"+
                            "vec3 n_normal=normalize(v_normal);"+
                            "vec3 lightvector = normalize(u_lightPosition - v_vertex);"+
                            "vec3 lightvector1 = normalize(u_lightPosition1 - v_vertex);"+
                            "vec3 lookvector = normalize(u_camera - v_vertex);"+
                            "float ambient=0.1;"+
                            "float k_diffuse=0.7;"+
                            "float k_specular=0.3;"+
                            "float diffuse = k_diffuse * max(dot(n_normal, lightvector), 0.0);"+
                            "float diffuse1 = k_diffuse * max(dot(n_normal, lightvector1), 0.0);"+

                            //"float diffuse = k_diffuse * dot(n_normal, lightvector);"+
                            //"float diffuse = k_diffuse;"+
                            "vec3 reflectvector = reflect(-lightvector, -n_normal);"+
                            "vec3 reflectvector1 = reflect(-lightvector1, -n_normal);"+
                            "float specular=k_specular*pow( max(dot(lookvector,reflectvector),0.0),40.0);"+
                            //"float specular=k_specular*pow(dot(lookvector,reflectvector),40.0);"+
                            //"float specular=k_specular"+
                            "vec4 one=vec4(1.0,1.0,1.0,1.0);"+
                            "vec4 lightColor=(ambient+diffuse+specular)*one;"+
                            "vec4 lightColor1=(ambient+diffuse1+specular)*one;"+
                            //"vec4 lightColor=one;"+
                            "gl_FragColor=lightColor;"+
                            "}";
            //создадим шейдерный объект
            mShader=new Shader(vertexShaderCode, fragmentShaderCode);
            //свяжем буфер вершин с атрибутом a_vertex в вершинном шейдере
            mShader.linkVertexBuffer(vertexBuffer);
            //свяжем буфер нормалей с атрибутом a_normal в вершинном шейдере
            mShader.linkNormalBuffer(normalBuffer);
            //связь атрибутов с буферами сохраняется до тех пор,
            //пока не будет уничтожен шейдерный объект
        }//конец метода
        //------------------------------------------------------------------------------------------

//метод, в котором выполняется рисование кадра
        public void onDrawFrame(GL10 unused) {          //передаем в шейдерный объект матрицу модели-вида-проекции

            //координаты камеры
            //xСamera=0.3f;
            //yCamera=1.7f;
            //zCamera+=0.1f;
        /*if (xСamera>=0 && zCamera<0){
            xСamera+=0.1f;
            zCamera+=0.1f;
        }
        if (xСamera>0 && zCamera>=0){
            xСamera-=0.1f;
            zCamera+=0.1f;
        }
        if (xСamera<=0 && zCamera>0){
            xСamera-=0.1f;
            zCamera-=0.1f;
        }
        if (xСamera<0 && zCamera<=0){
            xСamera+=0.1f;
            zCamera-=0.1f;
        }*///xxx+=xx;
            //yyy+=yy;


            //double time=System.currentTimeMillis();

                //xСamera = 2 * Rad * (float) Math.sin((xxx) / 10000);
               // zCamera = 2 * Rad * (float) Math.cos((xxx) / 10000);
            //yCamera = 2 * Rad * (float) Math.cos((yyy) / 10000);

            if(pov==true) {
                xСamera = (float) Math.sin(xxx / 10000) * (float) Math.cos(yyy / 10000) * 2 * Rad;
                zCamera = (float) Math.cos(xxx / 10000) * (float) Math.cos(yyy / 10000) * 2 * Rad;
                yCamera = (float) Math.sin(-yyy / 10000) * 2 * Rad;

                //xСamera=2*Rad*(float)Math.cos((yyy)/10000);
                //yCamera=2*Rad*(float)Math.sin((yyy)/10000);
                //yCamera=centr;


                //xLightPosition = (float) Math.sin(xxx / 10000) * (float) Math.cos(yyy / 10000) * 2 * Rad;
                ;
                //yLightPosition=5f;
                //zLightPosition = (float) Math.cos(xxx / 10000) * (float) Math.cos(yyy / 10000) * 2 * Rad;

                //yLightPosition = (float) Math.sin(-yyy / 10000) * 2 * Rad;
            }else{
                zCamera=Rad*2;
            }
            //yLightPosition=5*(float)Math.sin(j);

            j+=0.025;
            //zCamera=1.5f;



            far = Math.abs(Rad*4);
            // получаем матрицу проекции
            Matrix.frustumM(projectionMatrix, 0, left, right, bottom, top, near, far);
            //zСamera+=0.001f;
            //yCamera+=0.1f;
            //пусть камера смотрит на начало координат
            //и верх у камеры будет вдоль оси Y
            //зная координаты камеры получаем матрицу вида
            Matrix.setLookAtM(viewMatrix, 0, xСamera, yCamera, zCamera, centrx, centr, centrz, 0, 1, 0);
            // умножая матрицу вида на матрицу модели
            // получаем матрицу модели-вида
            Matrix.multiplyMM(modelViewMatrix, 0, viewMatrix, 0, modelMatrix, 0);

            Matrix.multiplyMM( modelViewProjectionMatrix, 0, projectionMatrix, 0, modelViewMatrix, 0);

            mShader.linkModelViewProjectionMatrix(modelViewProjectionMatrix);
            //передаем в шейдерный объект координаты камеры
            mShader.linkCamera(xСamera, yCamera, zCamera);
            //передаем в шейдерный объект координаты источника света
         mShader.linkLightSource(xLightPosition, yLightPosition, zLightPosition);
            //вычисляем координаты вершин
            //getVertex();
            //вычисляем координаты нормалей
            getNormal();
            //очищаем кадр
            glClear(GLES20.GL_COLOR_BUFFER_BIT
                    | GL_DEPTH_BUFFER_BIT);
            //рисуем поверхность
            GLES20.glDrawElements(GLES20.GL_TRIANGLE_STRIP, sizeindex,
                    GLES20.GL_UNSIGNED_SHORT, indexBuffer);
        }//конец метода
//------------------------------------------------------------------------------------------
    }//конец класса
//------------------------------------------------------------------------------------------

    private Bitmap createBitmapFromGLSurface(int x, int y, int w, int h, GL10 gl)
            throws OutOfMemoryError {
        int bitmapBuffer[] = new int[w * h];
        int bitmapSource[] = new int[w * h];
        IntBuffer intBuffer = IntBuffer.wrap(bitmapBuffer);
        intBuffer.position(0);

        try {
            gl.glReadPixels(x, y, w, h, GL10.GL_RGBA, GL10.GL_UNSIGNED_BYTE, intBuffer);
            int offset1, offset2;
            for (int i = 0; i < h; i++) {
                offset1 = i * w;
                offset2 = (h - i - 1) * w;
                for (int j = 0; j < w; j++) {
                    int texturePixel = bitmapBuffer[offset1 + j];
                    int blue = (texturePixel >> 16) & 0xff;
                    int red = (texturePixel << 16) & 0x00ff0000;
                    int pixel = (texturePixel & 0xff00ff00) | red | blue;
                    bitmapSource[offset2 + j] = pixel;
                }
            }
        } catch (GLException e) {
            return null;
        }

        return Bitmap.createBitmap(bitmapSource, w, h, Bitmap.Config.ARGB_8888);
    }

}
