package com.DandB.graph;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Splash2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(this, Graph.class);
        String min = getIntent().getExtras().getString("min");
        String max = getIntent().getExtras().getString("max");
        String formul = getIntent().getExtras().getString("formula");
        intent.putExtra("formula", formul);
        intent.putExtra("min", min);
        intent.putExtra("max", max);
        startActivity(intent);
        finish();
    }
}
